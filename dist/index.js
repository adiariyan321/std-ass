'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _db = require('./config/db');

var _db2 = _interopRequireDefault(_db);

var _middleware = require('./config/middleware');

var _middleware2 = _interopRequireDefault(_middleware);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _module = require('./module');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express2.default)();
app.use((0, _cors2.default)());
(0, _db2.default)();

(0, _middleware2.default)(app);

app.use('/api', [_module.UserRoutes, _module.UserAccessRoutes, _module.PreventiveRoutes, _module.BreakdownRoutes, _module.DispatchRoutes, _module.ToolsRoutes, _module.ProductionRoutes, _module.VariantRoutes, _module.LineRoutes, _module.ShiftRoutes, _module.MoldRoutes, _module.Changeover]);

const PORT = process.env.PORT || 3001;

app.listen(PORT, err => {
    if (err) {
        console.error(err);
    } else {
        console.log(`App listen to port: ${PORT}`);
    }
});