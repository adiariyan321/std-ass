'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
const devConfig = {
    DB_URL: 'mongodb://localhost/PHRecords'
};

exports.default = devConfig;