'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _controller = require('./controller');

var VariantController = _interopRequireWildcard(_controller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = new _express.Router();

routes.get('/variants', VariantController.getAllvariants);
routes.post('/variants/check', VariantController.checkvariants);
routes.post('/variants/getvariant', VariantController.getvariant);
routes.post('/variants/addvariant', VariantController.AddVariants);
routes.put('/variants/update', VariantController.updatevariants);
routes.delete('/variants/deletevariant', VariantController.Deletevariants);
exports.default = routes;