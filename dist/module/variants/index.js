'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.VariantRoutes = undefined;

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.VariantRoutes = _routes2.default;