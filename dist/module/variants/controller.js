'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Deletevariants = exports.updatevariants = exports.AddVariants = exports.checkvariants = exports.getvariant = exports.getAllvariants = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAllvariants = exports.getAllvariants = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newline = await _model2.default.find({});
        console.log('lines', newline);
        return res.status(200).json({ tool: newline });
        // return res.send(JSON.stringify({line: newline}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.' });
    }
};

const getvariant = exports.getvariant = async (req, res) => {
    try {
        const variantInfo = req.body;
        const newvariant = await variant.findOne({ email: variantInfo.email, password: variantInfo.password });
        console.log(newvariant);
        if (!newvariant) {
            return res.status(404).json({ error: true, message: 'variant not found' });
        } else {
            return res.status(200).json({ success: true, variant: newvariant });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find variants.' });
    }
};
const checkvariants = exports.checkvariants = async (req, res) => {
    try {
        const variantInfo = req.body;
        const newvariant = await variant.findOne({ email: variantInfo.email, password: variantInfo.password });
        console.log(newvariant);
        if (!newvariant) {
            return res.status(404).json({ error: true, message: "email or password is incorrect" });
        } else {
            return res.status(200).json({ success: true, variant: newvariant });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: "variant is not exist" });
    }
};
const AddVariants = exports.AddVariants = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ variantId: req.body.variantId });
        if (!usr) {
            const newissue = await _model2.default.createVariant(req.body);
            if (!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, token: '', user: newissue });
        }
        return res.status(200).json({ success: true, token: '', user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const updatevariants = exports.updatevariants = async (req, res) => {
    try {
        var usr = await variant.findOne({ email: req.body.email });
        if (usr) {
            const newvariant = await variant.updatevariant(req.body);
            return res.status(200).json({ variant: newvariant });
        }
        //        return res.status(200).json( {variant: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find variants.' });
    }
};
const Deletevariants = exports.Deletevariants = async (req, res) => {
    try {
        var usr = await variant.findOne({ email: req.body.email });
        console.log("pkb");
        if (usr) {
            const newvariant = await variant.removevariant(req.body);
            return res.status(200).json({ variant: newvariant });
        }
        return res.status(200).json({ variant: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find variants.' });
    }
};