'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const VariantsSchema = new _mongoose.Schema({
    variantId: {
        type: String,
        required: true,
        unique: true
    },
    variantName: String,
    cycleTime: String,
    isActive: String,
    materialNo: String
}, { timestamps: true });

VariantsSchema.statics.createVariant = async function (args) {
    try {
        const variantnew = await this.findOne({ variantId: args.variantId });
        if (!variantnew) {
            const variant = await this.create(args);
            return await this.findOne({ variantId: args.variantId });
        }
        return variantnew;
    } catch ({ error }) {
        return error;
    }
};

VariantsSchema.statics.updatevariant = async function (args) {
    try {
        const variantnew = await this.findOne({ email: args.email });
        if (variantnew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(variantnew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return variantnew;
    } catch ({ error }) {
        return error;
    }
};

VariantsSchema.statics.removevariant = async function (args) {
    try {
        const variantnew = await this.findOne({ email: args.email });
        if (variantnew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return variantnew;
    } catch ({ error }) {
        return error;
    }
};

exports.default = _mongoose2.default.model('Variants', VariantsSchema);