'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DeleteIssue = exports.updateIssue = exports.AdddIssue = exports.AddIssue = exports.getReports = exports.getAllIssues = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAllIssues = exports.getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newUser = await _model2.default.find({});
        console.log('Users', newUser);
        return res.status(200).json({ prev: newUser });
        // return res.send(JSON.stringify({user: newUser}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const getReports = exports.getReports = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        const prevreport = await _model2.default.find({ startDate: { $gt: startTime, $lt: endTime } });
        console.log(prevreport);
        return res.status(200).json({ success: true, prevreport: prevreport });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

const AddIssue = exports.AddIssue = async (req, res) => {
    try {
        // var iss=await Preventive.findOne({startDate:req.body.startDate});
        // if(!iss){
        const newissue = await _model2.default.createIssue(req.body);
        if (!newissue) {
            return res.status(405).json({ error: true, message: 'Error to create user.' });
        }
        return res.status(200).json({ success: true, token: '', user: newissue });
        // }
        // return res.status(200).json({success: true, token: '', user: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const AdddIssue = exports.AdddIssue = async (req, res) => {
    try {
        var iss = await _model2.default.findOne({ startDate: req.body.startDate });
        if (!iss) {
            const newissue = await _model2.default.createeIssue(req.body);
            if (!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, token: '', user: newissue });
        }
        return res.status(200).json({ success: true, token: '', user: iss });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const updateIssue = exports.updateIssue = async (req, res) => {
    try {
        var iss = await _model2.default.findOne({ email: req.body.email });
        if (iss) {
            const newissue = await _model2.default.updateUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        //        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const DeleteIssue = exports.DeleteIssue = async (req, res) => {
    try {
        var iss = await _model2.default.findOne({ email: req.body.email });
        console.log("pkb");
        if (iss) {
            const newissue = await _model2.default.removeUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        return res.status(200).json({ user: iss });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};