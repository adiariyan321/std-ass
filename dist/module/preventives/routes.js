'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _controller = require('./controller');

var PreventiveController = _interopRequireWildcard(_controller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = new _express.Router();

routes.get('/preventive', PreventiveController.getAllIssues);
routes.post('/preventive/getReports', PreventiveController.getReports);
routes.post('/preventive/addissue', PreventiveController.AddIssue);
routes.put('/preventive/updateissue', PreventiveController.updateIssue);
routes.delete('/preventive/deleteissue', PreventiveController.DeleteIssue);
exports.default = routes;