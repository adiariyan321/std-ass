'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _controller = require('./controller');

var ProductionController = _interopRequireWildcard(_controller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = new _express.Router();

routes.get('/Production', ProductionController.getAllProductionPlans);
routes.post('/Production/addissue', ProductionController.AddIssue);
routes.put('/Production/updateissue', ProductionController.updateIssue);
routes.delete('/Production/deleteissue', ProductionController.DeleteIssue);
routes.post('/Production/check', ProductionController.checkIssue);
routes.post('/Production/getReport', ProductionController.getReport);
exports.default = routes;