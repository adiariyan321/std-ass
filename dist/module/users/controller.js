'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DeleteUsers = exports.updateUsers = exports.AddUsers = exports.checkUsers = exports.getUser = exports.getAllUsers = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAllUsers = exports.getAllUsers = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newUser = await _model2.default.find({});
        console.log('Users', newUser);
        return res.status(200).json({ user: newUser });
        // return res.send(JSON.stringify({user: newUser}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const getUser = exports.getUser = async (req, res) => {
    try {
        const userInfo = req.body;
        const newUser = await _model2.default.findOne({ email: userInfo.email, password: userInfo.password });
        console.log(newUser);
        if (!newUser) {
            return res.status(404).json({ error: true, message: 'User not found' });
        } else {
            return res.status(200).json({ success: true, user: newUser });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const checkUsers = exports.checkUsers = async (req, res) => {
    try {
        const userInfo = req.body;
        const newUser = await _model2.default.findOne({ email: userInfo.email, password: userInfo.password });
        console.log(newUser);
        if (!newUser) {
            return res.status(404).json({ error: true, message: "email or password is incorrect" });
        } else {
            return res.status(200).json({ success: true, user: newUser });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: "user is not exist" });
    }
};
const AddUsers = exports.AddUsers = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        if (!usr) {
            const newuser = await _model2.default.createUser(req.body);
            if (!newuser) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, user: newuser });
        }
        return res.status(405).json({ error: true, message: "Email is already exist" });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'user is not found' });
    }
};
const updateUsers = exports.updateUsers = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        if (usr) {
            const newuser = await _model2.default.updateUser(req.body);
            return res.status(200).json({ user: newuser });
        }
        //        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const DeleteUsers = exports.DeleteUsers = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        console.log("pkb");
        if (usr) {
            const newuser = await _model2.default.removeUser(req.body);
            return res.status(200).json({ user: newuser });
        }
        return res.status(200).json({ user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};