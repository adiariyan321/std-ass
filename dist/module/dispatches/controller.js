'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DeleteIssue = exports.updateIssue = exports.AddDispatch = exports.getAllIssues = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAllIssues = exports.getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        return res.status(200).json({ user: await Preventive.find({}) });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const AddDispatch = exports.AddDispatch = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ dispatchId: req.body.dispatchId });
        if (!usr) {
            const newissue = await _model2.default.createDispatch(req.body);
            if (!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, token: '', Dispatch: newissue });
        }
        return res.status(200).json({ success: false, token: '', message: "this barcode number already exists" });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const updateIssue = exports.updateIssue = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        if (usr) {
            const newissue = await _model2.default.updateUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        //        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const DeleteIssue = exports.DeleteIssue = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        console.log("pkb");
        if (usr) {
            const newissue = await _model2.default.removeUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        return res.status(200).json({ user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};