'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ChangeoverSchema = new _mongoose.Schema({
    issueId: {
        type: String,
        unique: true,
        required: true
    },
    toolId: String,
    ticketId: String,
    description: String,
    isClosed: String,
    createdDate: String,
    closedDate: String,
    prsentShots: String,
    status: String,
    lineId: String,
    StationId: String,
    toolName: String,
    errorDescription: String
}, { timestamps: true });

ChangeoverSchema.statics.createIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ issueId: args.issueId });
        if (!issuenew) {
            const user = await this.create(args);
            return await this.findOne({ issueId: args.issueId });
        }
        return issuenew;
    } catch ({ error }) {
        return error;
    }
};

ChangeoverSchema.statics.updateIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ email: args.email });
        if (issuenew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(issuenew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return usernew;
    } catch ({ error }) {
        return error;
    }
};

ChangeoverSchema.statics.removeIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ email: args.email });
        if (issuenew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return issuenew;
    } catch ({ error }) {
        return error;
    }
};

exports.default = _mongoose2.default.model('Changeover', ChangeoverSchema);