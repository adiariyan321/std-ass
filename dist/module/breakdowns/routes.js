'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _controller = require('./controller');

var BreakdownController = _interopRequireWildcard(_controller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = new _express.Router();

routes.get('/Breakdown', BreakdownController.getAllIssues);
routes.post('/Breakdown/getReports', BreakdownController.getReports);
routes.post('/Breakdown/addissue', BreakdownController.AddIssue);
routes.put('/Breakdown/updateissue', BreakdownController.updateIssue);
routes.delete('/Breakdown/deleteissue', BreakdownController.DeleteIssue);

exports.default = routes;