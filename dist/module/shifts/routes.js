'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _controller = require('./controller');

var ShiftController = _interopRequireWildcard(_controller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = new _express.Router();

routes.get('/Shift', ShiftController.getAllShiftPlans);
routes.post('/Shift/addissue', ShiftController.AddIssue);
routes.put('/Shift/updateissue', ShiftController.updateIssue);
routes.delete('/Shift/deleteissue', ShiftController.DeleteIssue);
routes.post('/Shift/check', ShiftController.checkIssue);
exports.default = routes;