'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ShiftSchema = new _mongoose.Schema({
    shiftName: {
        type: String,
        required: true,
        unique: true
    },
    startTime: String,
    endTime: String
}, { timestamps: true });

ShiftSchema.statics.findProdPlans = async function () {
    try {
        const newPlans = await this.find({});
        const Tool = _mongoose2.default.model('Tools');
        var tools = await Tool.find({}, { _id: 0, toolId: 1 });
        var materialNo = await this.find({}, { _id: 0, materialNo: 1 });
        const Variant = _mongoose2.default.model('Variants');
        var variants = await Variant.find({}, { _id: 0, variantName: 1 });
        const Line = _mongoose2.default.model('Lines');
        var lines = await Line.find({}, { _id: 0, lineName: 1 });

        return { newPlans, tools, materialNo, variants, lines };
    } catch ({ error }) {
        return error;
    }
};

ShiftSchema.statics.createIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ shiftName: args.shiftName });
        if (!issuenew) {
            const newPlans = await this.create(args);
            return await this.findOne({ shiftName: args.shiftName });
        }
        return issuenew;
    } catch ({ error }) {
        return error;
    }
};

ShiftSchema.statics.updateIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ workOderNumber: args.workOderNumber });
        if (issuenew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(issuenew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return usernew;
    } catch ({ error }) {
        return error;
    }
};

ShiftSchema.statics.removeIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ email: args.email });
        if (issuenew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return issuenew;
    } catch ({ error }) {
        return error;
    }
};

exports.default = _mongoose2.default.model('Shift', ShiftSchema);