'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _controller = require('./controller');

var MoldController = _interopRequireWildcard(_controller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = new _express.Router();

routes.get('/Mold', MoldController.getAllMoldPlans);
routes.post('/Mold/addissue', MoldController.AddMold);
routes.put('/Mold/updateissue', MoldController.updateIssue);
routes.delete('/Mold/deleteissue', MoldController.DeleteIssue);
routes.post('/Mold/check', MoldController.checkIssue);
routes.post('/Mold/getMoldData', MoldController.getMoldData);
routes.post('/Mold/getMoldDatashift', MoldController.getMoldDataShift);
exports.default = routes;