'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LinesSchema = new _mongoose.Schema({
    lineId: {
        type: String,
        required: true,
        unique: true
    },
    lineName: String,
    cycleTime: String,
    isActive: String
}, { timestamps: true });

LinesSchema.statics.createline = async function (args) {
    try {
        const linenew = await this.findOne({ lineId: args.lineId });
        if (!linenew) {
            const line = await this.create(args);
            return await this.findOne({ lineId: args.lineId });
        }
        return linenew;
    } catch ({ error }) {
        return error;
    }
};

LinesSchema.statics.updateline = async function (args) {
    try {
        const linenew = await this.findOne({ email: args.email });
        if (linenew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(linenew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return linenew;
    } catch ({ error }) {
        return error;
    }
};

LinesSchema.statics.removeline = async function (args) {
    try {
        const linenew = await this.findOne({ email: args.email });
        if (linenew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return linenew;
    } catch ({ error }) {
        return error;
    }
};

exports.default = _mongoose2.default.model('Lines', LinesSchema);