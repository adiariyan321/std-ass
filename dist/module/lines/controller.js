'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Deletelines = exports.updatelines = exports.Addlines = exports.Addlines1 = exports.checklines = exports.getline = exports.getAlllines = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAlllines = exports.getAlllines = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newline = await _model2.default.find({});
        console.log('lines', newline);
        return res.status(200).json({ line: newline });
        // return res.send(JSON.stringify({line: newline}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.' });
    }
};

const getline = exports.getline = async (req, res) => {
    try {
        const lineInfo = req.body;
        const newline = await line.findOne({ email: lineInfo.email, password: lineInfo.password });
        console.log(newline);
        if (!newline) {
            return res.status(404).json({ error: true, message: 'line not found' });
        } else {
            return res.status(200).json({ success: true, line: newline });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.' });
    }
};
const checklines = exports.checklines = async (req, res) => {
    try {
        const lineInfo = req.body;
        const newline = await line.findOne({ email: lineInfo.email, password: lineInfo.password });
        console.log(newline);
        if (!newline) {
            return res.status(404).json({ error: true, message: "email or password is incorrect" });
        } else {
            return res.status(200).json({ success: true, line: newline });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: "line is not exist" });
    }
};
const Addlines1 = exports.Addlines1 = async (req, res) => {
    try {
        var usr = await line.findOne({ lineId: req.body.lineId });
        if (!usr) {
            const newline = await line.createline(req.body);
            if (!newline) {
                return res.status(405).json({ error: true, message: 'Error to create line.' });
            }
            return res.status(200).json({ success: true, line: newline });
        }
        return res.status(405).json({ error: true, message: "Email is already exist" });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'line is not found' });
    }
};

const Addlines = exports.Addlines = async (req, res) => {
    try {
        var usr = await line.findOne({ lineId: req.body.lineId });
        if (!usr) {
            const newissue = await line.createline(req.body);
            if (!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, token: '', user: newissue });
        }
        return res.status(200).json({ success: true, token: '', user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const updatelines = exports.updatelines = async (req, res) => {
    try {
        var usr = await line.findOne({ email: req.body.email });
        if (usr) {
            const newline = await line.updateline(req.body);
            return res.status(200).json({ line: newline });
        }
        //        return res.status(200).json( {line: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.' });
    }
};
const Deletelines = exports.Deletelines = async (req, res) => {
    try {
        var usr = await line.findOne({ email: req.body.email });
        console.log("pkb");
        if (usr) {
            const newline = await line.removeline(req.body);
            return res.status(200).json({ line: newline });
        }
        return res.status(200).json({ line: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.' });
    }
};