'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DeleteIssue = exports.updateIssue = exports.AddIssue = exports.getReports = exports.getAllIssues = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAllIssues = exports.getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newline = await _model2.default.find({});
        console.log('lines', newline);
        return res.status(200).json({ tool: newline });
        // return res.send(JSON.stringify({line: newline}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.' });
    }
};

const getReports = exports.getReports = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        const toolreport = await _model2.default.find({ startDate: { $gt: startTime, $lt: endTime } });
        console.log(toolreport);
        return res.status(200).json({ success: true, toolreport: toolreport });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

const AddIssue = exports.AddIssue = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ toolId: req.body.toolId });
        if (!usr) {
            const newissue = await _model2.default.createUser(req.body);
            if (!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, token: '', user: newissue });
        }
        return res.status(200).json({ success: true, token: '', user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const updateIssue = exports.updateIssue = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        if (usr) {
            const newissue = await _model2.default.updateUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        //        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const DeleteIssue = exports.DeleteIssue = async (req, res) => {
    try {
        var usr = await _model2.default.findOne({ email: req.body.email });
        console.log("pkb");
        if (usr) {
            const newissue = await _model2.default.removeUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        return res.status(200).json({ user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};