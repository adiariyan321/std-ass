'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ToolsSchema = new _mongoose.Schema({
    toolId: {
        type: String,
        required: true,
        unique: true
    },
    tollName: String,
    createdDate: String,
    startDate: String,
    lifespanWarning: String,
    lifespanThreshold: String,
    lifespanBlocking: String,
    preventiveWarning: String,
    preventiveThreshold: String,
    preventiveBlocking: String,
    cavities: String,
    isActive: String,
    cavityNumber: String,
    lineId: String,
    closedDate: String,
    status: String,
    noofShots: String,
    issueNoticed: String,
    correctiveAction: String,
    reopendBy: String,
    hrsofWorkdon: String,
    inspectionItem: String,
    requirement: String,
    isRequired: String
}, { timestamps: true });

ToolsSchema.statics.createUser = async function (args) {
    try {
        const usernew = await this.findOne({ toolId: args.toolId });
        if (!usernew) {
            const user = await this.create(args);
            return await this.findOne({ toolId: args.toolId });
        }
        return usernew;
    } catch ({ error }) {
        return error;
    }
};

ToolsSchema.statics.updateUser = async function (args) {
    try {
        const usernew = await this.findOne({ email: args.email });
        if (usernew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(usernew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return usernew;
    } catch ({ error }) {
        return error;
    }
};

ToolsSchema.statics.removeUser = async function (args) {
    try {
        const usernew = await this.findOne({ email: args.email });
        if (usernew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return usernew;
    } catch ({ error }) {
        return error;
    }
};

exports.default = _mongoose2.default.model('Tools', ToolsSchema);