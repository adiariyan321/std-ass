'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DeleteUsers = exports.updateUsers = exports.AddUsers = exports.getAllUsers = undefined;

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getAllUsers = exports.getAllUsers = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newUser = await _model2.default.find({});
        console.log('Users', newUser);
        return res.status(200).json({ user: newUser });
        // return res.send(JSON.stringify({user: newUser}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};

const AddUsers = exports.AddUsers = async (req, res) => {
    try {
        var usr = await UserAccessSchema.findOne({ email: req.body.email });
        if (!usr) {
            const newuser = await UserAccessSchema.createUser(req.body);
            if (!newuser) {
                return res.status(405).json({ error: true, message: 'Error to create user.' });
            }
            return res.status(200).json({ success: true, token: '', user: newuser });
        }
        return res.status(200).json({ success: true, token: '', user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const updateUsers = exports.updateUsers = async (req, res) => {
    try {
        var usr = await UserAccessSchema.findOne({ email: req.body.email });
        if (usr) {
            const newuser = await UserAccessSchema.updateUser(req.body);
            return res.status(200).json({ user: newuser });
        }
        //        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
const DeleteUsers = exports.DeleteUsers = async (req, res) => {
    try {
        var usr = await UserAccessSchema.findOne({ email: req.body.email });
        console.log("pkb");
        if (usr) {
            const newuser = await UserAccessSchema.removeUser(req.body);
            return res.status(200).json({ user: newuser });
        }
        return res.status(200).json({ user: usr });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};