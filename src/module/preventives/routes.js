import express, { Router } from 'express';

import * as PreventiveController from './controller';

const routes = new Router();

routes.get('/preventive', PreventiveController.getAllIssues);
routes.post('/preventive/getReports', PreventiveController.getReports);
routes.post('/preventive/addissue', PreventiveController.AddIssue);
routes.put('/preventive/updateissue',PreventiveController.updateIssue);
routes.delete('/preventive/deleteissue',PreventiveController.DeleteIssue);
export default routes;
  