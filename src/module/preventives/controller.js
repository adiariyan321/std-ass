import Preventive from './model';

export const getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newUser = await Preventive.find({});
        console.log('Users', newUser);
        return res.status(200).json({ prev: newUser });
       // return res.send(JSON.stringify({user: newUser}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const getReports = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        const prevreport = await Preventive.find({ startDate: {$gt: startTime , $lt: endTime } });
        console.log(prevreport);
        return res.status(200).json({ success: true, prevreport: prevreport });
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

export const AddIssue = async (req, res) => {
    try {  
        // var iss=await Preventive.findOne({startDate:req.body.startDate});
        // if(!iss){
            const newissue = await Preventive.createIssue(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', user: newissue} );
        // }
        // return res.status(200).json({success: true, token: '', user: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const AdddIssue = async (req, res) => {
    try {  
        var iss=await Preventive.findOne({startDate:req.body.startDate});
        if(!iss){
            const newissue = await Preventive.createeIssue(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', user: newissue} );
        }
        return res.status(200).json({success: true, token: '', user: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const updateIssue = async (req, res) => {
    try {  
        var iss=await Preventive.findOne({email:req.body.email});
        if(iss){
            const newissue = await Preventive.updateUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const DeleteIssue = async (req, res) => {
    try {  
        var iss=await Preventive.findOne({email:req.body.email});
        console.log("pkb");
        if(iss){
            const newissue = await Preventive.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json({user:iss});
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};