import mongoose, { Schema } from 'mongoose';

const PreventiveSchema = new Schema ({
    issueId: {
        type: String,
        unique: true,
        required:true
    },
    toolId: String,
    description: String,
    isClosed: String,
    createdDate:String,
    closedDate: String,
    status: String,
    lineId:String,
    StationId:String,
    toolName:String,
    errorDescription:String,
    question:String,
    method:String,
    isRequired:String,
    startDate:String,
    endDate:String
},
{ timestamps: true }
);

PreventiveSchema.statics.createIssue = async function(args){
    try {
        //const issuenew = await this.findOne({startDate: args.startDate});
        // if(!issuenew)
        // {
            const user = await this.create(args);
            return user;
        // }
        // return issuenew;
    } catch ({error}) {
        return error;
    }
};



PreventiveSchema.statics.createeIssue = async function(args){
    try {
        const issuenew = await this.findOne({startDate: args.startDate});
        if(!issuenew)
        {
            const user = await this.create(args);
            return await this.findOne({startDate: args.startDate});
        }
        return issuenew;
    } catch ({error}) {
        return error;
    }
};

PreventiveSchema.statics.updateIssue = async function(args){
    try {
            const issuenew = await this.findOne({email: args.email});
            if(issuenew)
            {
                console.log(args);
                const usr = await this.findByIdAndUpdate(issuenew._id, {$set:args});
                console.log(usr);
                return usr;
                //    return await this.findOne({email: args.email});
            }
        //    return usernew;
    } catch ({error}) {
        return error;
    }
};

PreventiveSchema.statics.removeIssue = async function(args){
    try {
        const issuenew = await this.findOne({email: args.email});
        if(issuenew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return issuenew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('Preventive', PreventiveSchema);
