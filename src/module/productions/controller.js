import Production from './model';

export const getAllProductionPlans = async (req, res) => {
    try {
        var {newPlans, tools,variants,materials, lines} = await Production.findProdPlans();
        console.log(newPlans, tools,variants,materials, lines);
         return res.status(200).json({ success: true, productionplan: newPlans, tools: tools,
                            variants:variants,materialNo:materials, lines:lines,  });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const getReport = async (req, res) => {
    try {
        const prodReportInfo = req.body;
        //console.log(prodReportInfo.startDate, prodReportInfo.endDate);
        const prodReport = await Production.getReport();
        console.log(prodReport);
//        var {newPlans, tools,variants, lines} = await Production.findProdPlans();
    //    console.log(newPlans, tools,variants, lines);
        // return res.status(200).json({ success: true, productionplan: newPlans, tools: tools,
        //                     variants:variants, lines:lines,  });
        return true;

    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const checkIssue = async (req,res) => {
    try{
        const issueInfo = req.body;
        
        const newIssue = await User.findOne({startDate: issueInfo.startDate,endDate:issueInfo.endDate});
        if(!newIssue){
            return res.status(404).json({error:true,message:"Find Date is incorrect"});
        }
        else {
            return res.status(200).json({success:true,user:newIssue});
        }
    }
    catch(e) {
        return res.status(e.status).json({error:true,message:"Date is not exist"})
    }
}

export const AddIssue = async (req, res) => {
    try {  
        var iss=await Production.findOne({workOderNumber:req.body.workOderNumber});
        if(!iss){   
            const newissue = await Production.createIssue(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create the new Productionplan data.'});
            }
            return res.status(200).json({ success: true, token: '', productionplan: newissue} );
        }
        return res.status(200).json({success: false, token: '', message: "this workorder number already exists"} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find the production plan data.'});
    }
};
export const updateIssue = async (req, res) => {
    try {  
        var iss=await Production.findOne({workOderNumber:req.body.workOderNumber});
        if(iss){
            const newissue = await Production.updateIssue(req.body);
            return res.status(200).json( {success:true, productionplan: newissue} );
        }
      // return res.status(200).json( {success:false,productionplan: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteIssue = async (req, res) => {
    try {  
        var iss=await Production.findOne({email:req.body.workOderNumber});
        console.log("pkb");
        if(iss){
            const newissue = await Production.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json({user:iss});
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};