import express, { Router } from 'express';

import * as ProductionController from './controller';

const routes = new Router();

routes.get('/Production', ProductionController.getAllProductionPlans);
routes.post('/Production/addissue', ProductionController.AddIssue);
routes.put('/Production/updateissue',ProductionController.updateIssue);
routes.delete('/Production/deleteissue',ProductionController.DeleteIssue);
routes.post('/Production/check',ProductionController.checkIssue);
routes.post('/Production/getReport',ProductionController.getReport);
export default routes;
