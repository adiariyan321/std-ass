import mongoose, { Schema } from 'mongoose';

const ProductionSchema = new Schema ({
    workOderNumber:{
        type:String,
        required:true,
        unique:true
    },
    userId:String,
    lineName:String,
    materialNo:String,
    variantName:String,
    toolId:String,
    cavity:String,
    datePlanned:Date,
    startTime:Date,
    endTime:Date,
    qtyPlanned:Number,
},
{ timestamps: true }
);


ProductionSchema.statics.findProdPlans = async function(){
    try {
        const newPlans = await this.find({});
        const Tool = mongoose.model('Tools');
        var tools = await Tool.find({}, { _id: 0, toolId: 1 });
        //const Variant = mongoose.model('Variants');
        // var variants = await Variant.find({},{_id:0, materialNo:1});
        const Variant = mongoose.model('Variants');
        var variants = await Variant.find({},{_id:0, variantName:1});
        var materials =await Variant.find({},{_id:0, materialNo:1})
        const Line = mongoose.model('Lines');
        var lines = await Line.find({},{_id:0, lineName:1});
        
        return {newPlans, tools,variants,materials, lines};
    } catch ({error}) {
        return error;
    }
};

ProductionSchema.statics.getReport = async function(args){
    try {
        const newProdReport = this.find({ 
        //     datePlanned: { 
        //     $gt: args.startDate, 
        //     $lt: args.endDate 
        // }
    });
        // const newPlans = await this.find({});
        // const Tool = mongoose.model('Tools');
        // var tools = await Tool.find({}, { _id: 0, toolId: 1 });
        // //const Variant = mongoose.model('Variants');
        // // var variants = await Variant.find({},{_id:0, materialNo:1});
        // const Variant = mongoose.model('Variants');
        // var variants = await Variant.find({},{_id:0, variantName:1,materialNo:1});
        // const Line = mongoose.model('Lines');
        // var lines = await Line.find({},{_id:0, lineName:1});

        return newProdReport;
    } catch ({error}) {
        return error;
    }
};

ProductionSchema.statics.createIssue = async function(args){
    try {
        const issuenew = await this.findOne({workOderNumber: args.workOderNumber});
        if(!issuenew)
        {
            const newPlans = await this.create(args);
            return await this.findOne({workOderNumber: args.workOderNumber});
        }
        return issuenew;
    } catch ({error}) {
        return error;
    }
};

ProductionSchema.statics.updateIssue = async function(args){
    try {
            const issuenew = await this.findOne({workOderNumber: args.workOderNumber});
            if(issuenew)
            {
                console.log(args);
                const usr = await this.findByIdAndUpdate(issuenew._id, {$set:args});
                console.log(usr);
                return usr;
                //    return await this.findOne({email: args.email});
            }
        //    return usernew;
    } catch ({error}) {
        return error;
    }
};

ProductionSchema.statics.removeIssue = async function(args){
    try {
        const issuenew = await this.findOne({email: args.email});
        if(issuenew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return issuenew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('Production', ProductionSchema);
