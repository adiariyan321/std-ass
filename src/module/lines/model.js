import mongoose, { Schema } from 'mongoose';

const LinesSchema = new Schema ({
   lineId:{
    type:String,
    required:true,
    unique:true
   },
   lineName:String,
   cycleTime:String,
   isActive:String
},
{ timestamps: true }
);

LinesSchema.statics.createline = async function(args){
    try {
        const linenew = await this.findOne({lineId: args.lineId});
        if(!linenew)
        {
            const line = await this.create(args);
            return await this.findOne({lineId: args.lineId});
        }
        return linenew;
    } catch ({error}) {
        return error;
    }
};

LinesSchema.statics.updateline = async function(args){
    try {
            const linenew = await this.findOne({email: args.email});
            if(linenew)
            {
                console.log(args);
                const usr = await this.findByIdAndUpdate(linenew._id, {$set:args});
                console.log(usr);
                return usr;
                //    return await this.findOne({email: args.email});
            }
        //    return linenew;
    } catch ({error}) {
        return error;
    }
};


LinesSchema.statics.removeline = async function(args){
    try {
        const linenew = await this.findOne({email: args.email});
        if(linenew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return linenew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('Lines', LinesSchema);
