import express, { Router } from 'express';

import * as LineController from './controller';

const routes = new Router();

routes.get('/lines', LineController.getAlllines);
routes.post('/lines/check',LineController.checklines);
routes.post('/lines/getline', LineController.getline);
routes.post('/lines/addline', LineController.Addlines);
routes.put('/lines/update',LineController.updatelines);
routes.delete('/lines/deleteline',LineController.Deletelines);
export default routes;
  