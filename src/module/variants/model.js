import mongoose, { Schema } from 'mongoose';

const VariantsSchema = new Schema ({
    variantId:{
        type:String,
        required:true,
        unique:true
    },
    variantName:String,
    cycleTime:String,
    isActive:String,
    materialNo : String
},
{ timestamps: true }
);

VariantsSchema.statics.createVariant = async function(args){
    try {
        const variantnew = await this.findOne({variantId: args.variantId});
        if(!variantnew)
        {
            const variant = await this.create(args);
            return await this.findOne({variantId: args.variantId});
        }
        return variantnew;
    } catch ({error}) {
        return error;
    }
};

VariantsSchema.statics.updatevariant = async function(args){
    try {
            const variantnew = await this.findOne({email: args.email});
            if(variantnew)
            {
                console.log(args);
                const usr = await this.findByIdAndUpdate(variantnew._id, {$set:args});
                console.log(usr);
                return usr;
                //    return await this.findOne({email: args.email});
            }
        //    return variantnew;
    } catch ({error}) {
        return error;
    }
};


VariantsSchema.statics.removevariant = async function(args){
    try {
        const variantnew = await this.findOne({email: args.email});
        if(variantnew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return variantnew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('Variants', VariantsSchema);
