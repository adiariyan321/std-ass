import express, { Router } from 'express';

import * as VariantController from './controller';

const routes = new Router();

routes.get('/variants', VariantController.getAllvariants);
routes.post('/variants/check',VariantController.checkvariants);
routes.post('/variants/getvariant', VariantController.getvariant);
routes.post('/variants/addvariant', VariantController.AddVariants);
routes.put('/variants/update',VariantController.updatevariants);
routes.delete('/variants/deletevariant',VariantController.Deletevariants);
export default routes;
  