import Tools from './model';

export const getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newline = await Tools.find({});
        console.log('lines', newline);
        return res.status(200).json({ tool: newline });
       // return res.send(JSON.stringify({line: newline}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find lines.'});
    }
};


export const getReports = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        const toolreport = await Tools.find({ startDate: {$gt: startTime , $lt: endTime } });
        console.log(toolreport);
        return res.status(200).json({ success: true, toolreport: toolreport });
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};


export const AddIssue = async (req, res) => {
    try {  
        var usr=await Tools.findOne({toolId:req.body.toolId});
        if(!usr){
            const newissue = await Tools.createUser(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', user: newissue} );
        }
        return res.status(200).json({success: true, token: '', user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const updateIssue = async (req, res) => {
    try {  
        var usr=await Tools.findOne({email:req.body.email});
        if(usr){
            const newissue = await Tools.updateUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteIssue = async (req, res) => {
    try {  
        var usr=await Tools.findOne({email:req.body.email});
        console.log("pkb");
        if(usr){
            const newissue = await Tools.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};