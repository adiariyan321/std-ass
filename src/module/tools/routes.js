import express, { Router } from 'express';

import * as ToolsController from './controller';

const routes = new Router();

routes.get('/Tools', ToolsController.getAllIssues);
routes.post('/Tools/getReports', ToolsController.getReports);
routes.post('/Tools/addissue', ToolsController.AddIssue);
routes.put('/Tools/updateissue',ToolsController.updateIssue);
routes.delete('/Tools/deleteissue',ToolsController.DeleteIssue);
export default routes;
  