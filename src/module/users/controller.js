import User from './model';

export const getAllUsers = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newUser = await User.find({});
        console.log('Users', newUser);
        return res.status(200).json({ user: newUser });
       // return res.send(JSON.stringify({user: newUser}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};


export const getUser = async (req, res) => {
    try {
        const userInfo = req.body;
        const newUser = await User.findOne({ email: userInfo.email, password: userInfo.password });
        console.log(newUser);
        if(!newUser) {
            return res.status(404).json({ error: true, message: 'User not found' });
        } else {
            return res.status(200).json({ success: true, user: newUser });
        }
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const checkUsers = async (req,res) => {
    try{
        const userInfo = req.body;
        const newUser = await User.findOne({email: userInfo.email,password:userInfo.password});
        console.log(newUser);
        if(!newUser){
            return res.status(404).json({error:true,message:"email or password is incorrect"});
        }
        else {
            return res.status(200).json({success:true,user:newUser});
        }
    }
    catch(e) {
        return res.status(e.status).json({error:true,message:"user is not exist"})
    }
}
export const AddUsers = async (req, res) => {
    try {  
        var usr=await User.findOne({email:req.body.email});
        if(!usr){
            const newuser = await User.createUser(req.body);
            if(!newuser) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true,user: newuser} );
        }
        return res.status(405).json({error: true,message:"Email is already exist"} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'user is not found'});
    }
};
export const updateUsers = async (req, res) => {
    try {  
        var usr=await User.findOne({email:req.body.email});
        if(usr){
            const newuser = await User.updateUser(req.body);
            return res.status(200).json( {user: newuser} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteUsers = async (req, res) => {
    try {  
        var usr=await User.findOne({email:req.body.email});
        console.log("pkb");
        if(usr){
            const newuser = await User.removeUser(req.body);
            return res.status(200).json( {user: newuser} );
        }
        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};