import mongoose, { Schema } from 'mongoose';

const UserSchema = new Schema ({
    email: {
        type: String,
        unique: true,
        required:true
    },
    password: String,
    first_name: String,
    last_name:String,
    role:String,
    users:String,
    category:String,
    preventive_maintenance:Boolean,
    breakdown_maintenance:Boolean,
    tool_replacement:Boolean,
},
{ timestamps: true }
);

UserSchema.statics.createUser = async function(args){
    try {
        const usernew = await this.findOne({email: args.email});
        if(!usernew)
        {
            const user = await this.create(args);
            return await this.findOne({email: args.email});
        }
        return usernew;
    } catch ({error}) {
        return error;
    }
};

UserSchema.statics.updateUser = async function(args){
    try {
            const usernew = await this.findOne({email: args.email});
            if(usernew)
            {
                console.log(args);
                const usr = await this.findByIdAndUpdate(usernew._id, {$set:args});
                console.log(usr);
                return usr;
                //    return await this.findOne({email: args.email});
            }
        //    return usernew;
    } catch ({error}) {
        return error;
    }
};


UserSchema.statics.removeUser = async function(args){
    try {
        const usernew = await this.findOne({email: args.email});
        if(usernew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return usernew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('User', UserSchema);
