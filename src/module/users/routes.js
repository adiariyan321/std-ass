import express, { Router } from 'express';

import * as UserController from './controller';

const routes = new Router();

routes.get('/users', UserController.getAllUsers);
routes.post('/users/check',UserController.checkUsers);
routes.post('/users/getuser', UserController.getUser);
routes.post('/users/adduser', UserController.AddUsers);
routes.put('/users/update',UserController.updateUsers);
routes.delete('/users/deleteuser',UserController.DeleteUsers);
export default routes;
  