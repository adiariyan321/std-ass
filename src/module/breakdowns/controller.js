import Breakdown from './model';

export const getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        return res.status(200).json({ user: await Breakdown.find({}) });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const getReports = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        const breakreport = await Breakdown.find({ startDate: { $gt: startTime, $lt: endTime } });
        console.log(breakreport);
        return res.status(200).json({ success: true, breakreport: breakreport });
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

export const AddIssue = async (req, res) => {
    try {  
        var usr=await Breakdown.findOne({breakdownIssueId:req.body.breakdownIssueId});
        if(!usr){
            const newissue = await Breakdown.createUser(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', user: newissue} );
        }
        return res.status(200).json({success: true, token: '', user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const updateIssue = async (req, res) => {
    try {  
        var usr=await Breakdown.findOne({email:req.body.email});
        if(usr){
            const newissue = await Breakdown.updateUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteIssue = async (req, res) => {
    try {  
        var usr=await Breakdown.findOne({email:req.body.email});
        console.log("pkb");
        if(usr){
            const newissue = await Breakdown.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
