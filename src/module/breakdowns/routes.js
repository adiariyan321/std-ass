import express, { Router } from 'express';

import * as BreakdownController from './controller';

const routes = new Router();

routes.get('/Breakdown', BreakdownController.getAllIssues);
routes.post('/Breakdown/getReports', BreakdownController.getReports);
routes.post('/Breakdown/addissue', BreakdownController.AddIssue);
routes.put('/Breakdown/updateissue',BreakdownController.updateIssue);
routes.delete('/Breakdown/deleteissue',BreakdownController.DeleteIssue);

export default routes;
  