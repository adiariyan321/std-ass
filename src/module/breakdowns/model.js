import mongoose, { Schema } from 'mongoose';

const BreakdownSchema = new Schema ({
    breakdownIssueId:{
        type:String,
        required:true,
        unique:true
    },
    toolId:String,
    breakdownDate:String,
    errorDescription:String,
    remarks:String,
    presentShot:String,
    userId:String,
    status:String,
    targetDate:String,
    lineId:String,
    stationId:String,
    closeDate:String,
    failureReason:String,
    isRequired:String,
    StartDate:String,
    endDate:String
 },
{ timestamps: true }
);

BreakdownSchema.statics.createUser = async function(args){
    try {
        const usernew = await this.findOne({breakdownIssueId: args.breakdownIssueId});
        if(!usernew)
        {
            const user = await this.create(args);
            return await this.findOne({breakdownIssueId: args.breakdownIssueId});
        }
        return usernew;
    } catch ({error}) {
        return error;
    }
};

BreakdownSchema.statics.updateUser = async function(args){
    try {
        const usernew = await this.findOne({email: args.email});
        if(usernew)
        {
            console.log(args);
            const usr = await this.findByIdAndUpdate(usernew._id, { $set:args });
            console.log(usr);
            return usr;
        }
    } catch ({error}) {
        return error;
    }
};


BreakdownSchema.statics.removeUser = async function(args){
    try {
        const usernew = await this.findOne({email: args.email});
        if(usernew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return usernew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('Breakdown', BreakdownSchema);
