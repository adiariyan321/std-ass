import UserAccess from './model';

export const getAllUsers = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newUser = await UserAccess.find({});
        console.log('Users', newUser);
        return res.status(200).json({ user: newUser });
       // return res.send(JSON.stringify({user: newUser}));
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const AddUsers = async (req, res) => {
    try {  
        var usr=await UserAccessSchema.findOne({email:req.body.email});
        if(!usr){
            const newuser = await UserAccessSchema.createUser(req.body);
            if(!newuser) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', user: newuser} );
        }
        return res.status(200).json({success: true, token: '', user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const updateUsers = async (req, res) => {
    try {  
        var usr=await UserAccessSchema.findOne({email:req.body.email});
        if(usr){
            const newuser = await UserAccessSchema.updateUser(req.body);
            return res.status(200).json( {user: newuser} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteUsers = async (req, res) => {
    try {  
        var usr=await UserAccessSchema.findOne({email:req.body.email});
        console.log("pkb");
        if(usr){
            const newuser = await UserAccessSchema.removeUser(req.body);
            return res.status(200).json( {user: newuser} );
        }
        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};