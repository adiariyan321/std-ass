import express, { Router } from 'express';

import * as UserAccessController from './controller';

const routes = new Router();

routes.get('/usersaccess', UserAccessController.getAllUsers);
routes.post('/usersaccess/adduser', UserAccessController.AddUsers);
routes.put('/usersaccess/update',UserAccessController.updateUsers);
routes.delete('/usersaccess/deleteuser',UserAccessController.DeleteUsers);
export default routes;
  