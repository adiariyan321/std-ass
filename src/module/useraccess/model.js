import mongoose, { Schema } from 'mongoose';

const UserAccessSchema = new Schema ({
    accessId:{
        type:String,
        unique: true,
        required:true
    } ,
    category:String,
    roles:String,
    notifications: String,
    screens:String,
    features:String,
    featuresDescription:String
},
{ timestamps: true }
);

UserAccessSchema.statics.createUser = async function(args){
    try {
        const usernew = await this.findOne({email: args.email});
        if(!usernew)
        {
            const user = await this.create(args);
            return await this.findOne({email: args.email});
        }
        return usernew;
    } catch ({error}) {
        return error;
    }
};

UserAccessSchema.statics.updateUser = async function(args){
    try {
            const usernew = await this.findOne({email: args.email});
            if(usernew)
            {
                console.log(args);
                const usr = await this.findByIdAndUpdate(usernew._id, {$set:args});
                console.log(usr);
                return usr;
                //    return await this.findOne({email: args.email});
            }
        //    return usernew;
    } catch ({error}) {
        return error;
    }
};


UserAccessSchema.statics.removeUser = async function(args){
    try {
        const usernew = await this.findOne({email: args.email});
        if(usernew)
        {
             await this.remove(args);
             console.log("ok");
            return await this.findOne({email: args.email});
        }
        return usernew;
    } catch ({error}) {
        return error;
    }
};

export default mongoose.model('UserAccess', UserAccessSchema);
