import mongoose, { Schema } from 'mongoose';

const DispatchSchema = new Schema({
    dispatchId: {
        type: String,
        required: true,
        unique: true
    },
    variantId: String,
    variantName:String,
    quantity: String,
    resived:String,
    recieveddate: String,
    receivedby:String,
    dispatchDate: String,
    ponumber:String,
    itemNumber:String,
    partNumber:String,
    dispatchdateaptiv:String,
    pallets:String,
    shipmentplanNumber:String,
    dispatchplanaptiv:String,
    actual:String,
    gap:String
},
    { timestamps: true }
);

DispatchSchema.statics.createDispatch = async function (args) {
    try {
        const usernew = await this.findOne({ dispatchId: args.dispatchId });
        if (!usernew) {
            const user = await this.create(args);
            return await this.findOne({ dispatchId: args.dispatchId });
        }
        return usernew;
    } catch ({ error }) {
        return error;
    }
};

DispatchSchema.statics.updateUser = async function (args) {
    try {
        const usernew = await this.findOne({ email: args.email });
        if (usernew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(usernew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return usernew;
    } catch ({ error }) {
        return error;
    }
};


DispatchSchema.statics.removeUser = async function (args) {
    try {
        const usernew = await this.findOne({ email: args.email });
        if (usernew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return usernew;
    } catch ({ error }) {
        return error;
    }
};

export default mongoose.model('Dispatch', DispatchSchema);
