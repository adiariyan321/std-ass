import express, { Router } from 'express';

import * as DispatchController from './controller';

const routes = new Router();

routes.get('/Dispatch', DispatchController.getAllIssues);
routes.post('/Dispatch/adddispatch', DispatchController.AddDispatch);
routes.put('/Dispatch/updateissue',DispatchController.updateIssue);
routes.delete('/Dispatch/deleteissue',DispatchController.DeleteIssue);
export default routes;
  