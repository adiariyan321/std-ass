import Dispatch from './model';

export const getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        return res.status(200).json({ user: await Preventive.find({}) });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};

export const AddDispatch = async (req, res) => {
    try {  
        var usr=await Dispatch.findOne({dispatchId:req.body.dispatchId});
        if(!usr){
            const newissue = await Dispatch.createDispatch(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', Dispatch: newissue} );
        }
        return res.status(200).json({success: false, token: '', message: "this barcode number already exists"} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const updateIssue = async (req, res) => {
    try {  
        var usr=await Dispatch.findOne({email:req.body.email});
        if(usr){
            const newissue = await Dispatch.updateUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteIssue = async (req, res) => {
    try {  
        var usr=await Dispatch.findOne({email:req.body.email});
        console.log("pkb");
        if(usr){
            const newissue = await Dispatch.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};