import Shift from './model';

export const getAllShiftPlans = async (req, res) => {
    try {
        var {newPlans, tools, materialNo, variants, lines} = await Shift.findProdPlans();
        console.log(newPlans, tools, materialNo, variants, lines);
         return res.status(200).json({ success: true, Shiftplan: newPlans, tools: tools, 
            materialNo:materialNo,variants:variants, lines:lines });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const checkIssue = async (req,res) => {
    try{
        const issueInfo = req.body;
        const newIssue = await User.findOne({startDate: issueInfo.startDate,endDate:issueInfo.endDate});
        if(!newIssue){
            return res.status(404).json({error:true,message:"Find Date is incorrect"});
        }
        else {
            return res.status(200).json({success:true,user:newIssue});
        }
    }
    catch(e) {
        return res.status(e.status).json({error:true,message:"Date is not exist"})
    }
}

export const AddIssue = async (req, res) => {
    try {  
        var iss=await Shift.findOne({shiftName:req.body.shiftName});
        if(!iss){   
            const newissue = await Shift.createIssue(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create the new Shiftplan data.'});
            }
            return res.status(200).json({ success: true, token: '', Shiftplan: newissue} );
        }
        return res.status(200).json({success: false, token: '', message: "this workorder number already exists"} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find the Shift plan data.'});
    }
};
export const updateIssue = async (req, res) => {
    try {  
        var iss=await Shift.findOne({workOderNumber:req.body.workOderNumber});
        if(iss){
            const newissue = await Shift.updateIssue(req.body);
            return res.status(200).json( {success:true, Shiftplan: newissue} );
        }
      // return res.status(200).json( {success:false,Shiftplan: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteIssue = async (req, res) => {
    try {  
        var iss=await Shift.findOne({email:req.body.workOderNumber});
        console.log("pkb");
        if(iss){
            const newissue = await Shift.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json({user:iss});
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};