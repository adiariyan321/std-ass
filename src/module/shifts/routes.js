import express, { Router } from 'express';

import * as ShiftController from './controller';

const routes = new Router();

routes.get('/Shift', ShiftController.getAllShiftPlans);
routes.post('/Shift/addissue', ShiftController.AddIssue);
routes.put('/Shift/updateissue',ShiftController.updateIssue);
routes.delete('/Shift/deleteissue',ShiftController.DeleteIssue);
routes.post('/Shift/check',ShiftController.checkIssue);
export default routes;
