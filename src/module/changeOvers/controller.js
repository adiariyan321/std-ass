import Changeover from './model';

export const getAllIssues = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        return res.status(200).json({ user: await Changeover.find({}) });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};


export const getReports = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        const toolreport = await Changeover.find({ startDate: {$gt: startTime , $lt: endTime } });
        console.log(toolreport);
        return res.status(200).json({ success: true, toolreport: toolreport });
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};


export const AddIssue = async (req, res) => {
    try {  
        var iss=await Changeover.findOne({issueId:req.body.issueId});
        if(!iss){
            const newissue = await Changeover.createIssue(req.body);
            if(!newissue) {
                return res.status(405).json({ error: true, message: 'Error to create user.'});
            }
            return res.status(200).json({ success: true, token: '', user: newissue} );
        }
        return res.status(200).json({success: true, token: '', user: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const updateIssue = async (req, res) => {
    try {  
        var iss=await Changeover.findOne({email:req.body.email});
        if(iss){
            const newissue = await Changeover.updateUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
//        return res.status(200).json( {user: usr} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};
export const DeleteIssue = async (req, res) => {
    try {  
        var iss=await Changeover.findOne({email:req.body.email});
        console.log("pkb");
        if(iss){
            const newissue = await Changeover.removeUser(req.body);
            return res.status(200).json( {user: newissue} );
        }
        return res.status(200).json({user:iss});
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.'});
    }
};