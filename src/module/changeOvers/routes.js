import express, { Router } from 'express';

import * as ChangeoverController from './controller';

const routes = new Router();

routes.get('/changeover', ChangeoverController.getAllIssues);
routes.post('/changeover/addissue', ChangeoverController.AddIssue);
routes.put('/changeover/updateissue',ChangeoverController.updateIssue);
routes.delete('/changeover/deleteissue',ChangeoverController.DeleteIssue);
export default routes;
  