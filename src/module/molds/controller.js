import Mold from './model';

export const getAllMoldPlans = async (req, res) => {
    try {
        console.log('Request coming', req.body);
        const newMold = await Mold.find({});
        console.log('Users', newMold);
        return res.status(200).json({ mold: newMold });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

export const getMoldDataShift = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        var noShifts = Math.abs(new Date(endTime) - new Date(startTime)) / 36e5 / 8;
        let prodMold = [];
        const prodTimestamp = await Mold.find({ timestamp: { $gt: startTime + "T06:00:00", $lt: endTime + "T05:59:59" } });
        var startMoldTime = new Date(startTime);
        startMoldTime = new Date(startMoldTime.setHours(startMoldTime.getHours() - 2));
        var startPeriod = startMoldTime;
        var endPeriod = startMoldTime;
        for (var i = 1; i <= parseInt(noShifts, 10); i++) {
            let obj = {};
            startPeriod = new Date(startPeriod.setHours(startPeriod.getHours() + 8));;
            endPeriod = new Date(endPeriod.setHours(endPeriod.getHours() + 8));
            var prodActCount = 0;
            var prodDummyCount = 0;
            var tol;
            var varid;
            prodTimestamp.forEach(tim => {
                if (new Date(tim.timestamp) >= startPeriod && new Date(tim.timestamp) <= endPeriod) {
                    tol = tim.toolId;
                    varid = tim.variantId;
                    if (tim.shotType == 'Actual') {
                        prodActCount += 1;
                    } else if (tim.shotType == 'Dummy') {
                        prodDummyCount += 1;
                    }
                }
            });
            var k = i % 3;
            if (k == 0) k = 3;
            obj.Id = i;
            obj.shift = k;
            obj.moldActualCount = prodActCount;
            obj.moldDummyCount = prodDummyCount;
            obj.tolid = tol;
            obj.vrid = varid;
            prodMold.push(obj);
        }
        console.log(prodMold);
        return res.status(200).json({ success: true, moldShiftCount: prodMold });
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

export const getMoldData = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        var noShifts = Math.abs(new Date(endTime) - new Date(startTime)) / 36e5 / 8;
        let prodShiftMold = [];
        let prodDayMold = [];
        let prodMonthMold = [];
        const prodTimestamp = await Mold.find({ timestamp: { $gt: startTime + "T06:00:00", $lt: endTime + "T05:59:59" } });
        var startMoldTime = new Date(startTime);
        startMoldTime = new Date(startMoldTime.setHours(startMoldTime.getHours() - 2));
        var startPeriod = startMoldTime;
        var endPeriod = startMoldTime;
        var dayActCount = 0;
        var dayDummyCount = 0;
        var monthActCount = 0;
        var monthDummyCount = 0;
        var monthName = startMoldTime.getMonth();
        var changeTime = new Date(startTime);
        changeTime = new Date(changeTime.setHours(changeTime.getHours() - 24));
        for (var i = 1; i <= parseInt(noShifts, 10); i++) {
            startPeriod = new Date(startPeriod.setHours(startPeriod.getHours() + 8));;
            endPeriod = new Date(endPeriod.setHours(endPeriod.getHours() + 8));
            var prodActCount = 0;
            var prodDummyCount = 0;
            var tol;
            var varid;
            prodTimestamp.forEach(tim => {
                if (new Date(tim.timestamp) >= startPeriod && new Date(tim.timestamp) <= endPeriod) {
                    tol = tim.toolId;
                    varid = tim.variantId;
                    if (tim.shotType == 'Actual') {
                        prodActCount += 1;
                    } else if (tim.shotType == 'Dummy') {
                        prodDummyCount += 1;
                    }
                }
            });
            dayActCount += prodActCount;
            dayDummyCount += prodDummyCount;
    //        console.log('Month DataA: ', monthActCount);
            if(monthName == changeTime.getMonth()) {
                monthActCount += dayActCount;
                monthDummyCount += dayDummyCount;
  //              console.log('Month DataB: ', monthActCount);
            } else {
//                console.log('Month DataC: ', monthActCount);
                var objMonth ={}
                objMonth.Id = monthName ;
                objMonth.month = monthName;
                objMonth.moldActualCount = monthActCount;
                objMonth.moldDummyCount = monthDummyCount;
                objMonth.tolid = tol;
                objMonth.vrid = varid;
                prodMonthMold.push(objMonth);
                monthName = changeTime.getMonth();
                monthActCount = dayActCount;
                monthDummyCount = dayDummyCount;
            }
            var k = i % 3;
            if (k == 0) k = 3;
            var objShift = {};
            objShift.Id = i;
            objShift.shift = k;
            objShift.dayData = changeTime;
            objShift.moldActualCount = prodActCount;
            objShift.moldDummyCount = prodDummyCount;
            objShift.tolid = tol;
            objShift.vrid = varid;
            if (k == 3) {
                var objDay = {};
                objDay["Id"] = i / 3;
                objDay["dayData"] = changeTime;
                changeTime = new Date(changeTime.setHours(changeTime.getHours() + 24));
                objDay["Actual"] = dayActCount;
                objDay["Dummy"] = dayDummyCount;
                objDay.tolid = tol;
                objDay.vrid = varid;
                dayActCount = 0;
                dayDummyCount = 0;
                prodDayMold.push(objDay);
            }
            prodShiftMold.push(objShift);
        }
        var objMonth ={}
        objMonth.Id = monthName;
        objMonth.month = monthName;
        objMonth.moldActualCount = monthActCount;
        objMonth.moldDummyCount = monthDummyCount;
        objMonth.tolid = tol;
        objMonth.vrid = varid;
        prodMonthMold.push(objMonth);
    //    console.log('Obj Month: ', prodMonthMold);
        return res.status(200).json({ success: true, moldCount: {shiftData: prodShiftMold, dayData: prodDayMold, monthData: prodMonthMold }});
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find' });
    }
};

export const checkIssue = async (req, res) => {
    try {
        var startTime = req.body.startDate;
        var endTime = req.body.endDate;
        //const issueInfo = req.body;
        const newIssue = await Mold.findOne({ timestamp: { $gt: startTime + "T06:00:00", $lt: endTime + "T05:59:59" } });
        if (!newIssue) {
            return res.status(404).json({ error: true, message: "Find Date is incorrect" });
        }
        else {
            console.log("value",newIssue)
            return res.status(200).json({ success: true, mold: newIssue });
        }
    }
    catch (e) {
        return res.status(e.status).json({ error: true, message: "Date is not exist" })
    }
}

export const AddMold = async (req, res) => {
    try {
        var iss = await Mold.findOne({ timestamp: req.body.timestamp });
        if (!iss) {
            const newmold = await Mold.createMold(req.body);
            if (!newmold) {
                return res.status(405).json({ error: true, message: 'Error to create the new Moldplan data.' });
            }
            return res.status(200).json({ success: true, token: '', Moldplan: newmold });
        }
        return res.status(200).json({ success: false, token: '', message: "this timestamp already exists" });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find the Mold plan data.' });
    }
};
export const updateIssue = async (req, res) => {
    try {
        var iss = await Mold.findOne({ workOderNumber: req.body.workOderNumber });
        if (iss) {
            const newissue = await Mold.updateIssue(req.body);
            return res.status(200).json({ success: true, Moldplan: newissue });
        }
        // return res.status(200).json( {success:false,Moldplan: iss} );
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};
export const DeleteIssue = async (req, res) => {
    try {
        var iss = await Mold.findOne({ email: req.body.workOderNumber });
        console.log("pkb");
        if (iss) {
            const newissue = await Mold.removeUser(req.body);
            return res.status(200).json({ user: newissue });
        }
        return res.status(200).json({ user: iss });
    } catch (e) {
        return res.status(e.status).json({ error: true, message: 'Error to find users.' });
    }
};