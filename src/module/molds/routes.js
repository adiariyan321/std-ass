import express, { Router } from 'express';

import * as MoldController from './controller';

const routes = new Router();

routes.get('/Mold', MoldController.getAllMoldPlans);
routes.post('/Mold/addissue', MoldController.AddMold);
routes.put('/Mold/updateissue',MoldController.updateIssue);
routes.delete('/Mold/deleteissue',MoldController.DeleteIssue);
routes.post('/Mold/check',MoldController.checkIssue);
routes.post('/Mold/getMoldData',MoldController.getMoldData);
routes.post('/Mold/getMoldDatashift',MoldController.getMoldDataShift);
export default routes;