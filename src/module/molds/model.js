import mongoose, { Schema } from 'mongoose';

const MoldSchema = new Schema({
    timestamp: {
        type: String,
        required: true,
        unique: true
    },
    toolId: String,
    variantId: String,
    shotType: String
},
    { timestamps: true }
);

MoldSchema.statics.findProdPlans = async function () {
    try {
        const newPlans = await this.find({});
        const Tool = mongoose.model('Tools');
        var tools = await Tool.find({}, { _id: 0, toolId: 1 });
        var materialNo = await this.find({}, { _id: 0, materialNo: 1 });
        const Variant = mongoose.model('Variants');
        var variants = await Variant.find({}, { _id: 0, variantName: 1 });
        const Line = mongoose.model('Lines');
        var lines = await Line.find({}, { _id: 0, lineName: 1 });

        return { newPlans, tools, materialNo, variants, lines };
    } catch ({ error }) {
        return error;
    }
};

MoldSchema.statics.createMold = async function (args) {
    try {
        const issuenew = await this.findOne({ timestamp: args.timestamp });
        if (!issuenew) {
            const newPlans = await this.create(args);
            return await this.findOne({ timestamp: args.timestamp });
        }
        return issuenew;
    } catch ({ error }) {
        return error;
    }
};

MoldSchema.statics.updateIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ workOderNumber: args.workOderNumber });
        if (issuenew) {
            console.log(args);
            const usr = await this.findByIdAndUpdate(issuenew._id, { $set: args });
            console.log(usr);
            return usr;
            //    return await this.findOne({email: args.email});
        }
        //    return usernew;
    } catch ({ error }) {
        return error;
    }
};

MoldSchema.statics.removeIssue = async function (args) {
    try {
        const issuenew = await this.findOne({ email: args.email });
        if (issuenew) {
            await this.remove(args);
            console.log("ok");
            return await this.findOne({ email: args.email });
        }
        return issuenew;
    } catch ({ error }) {
        return error;
    }
};

export default mongoose.model('Mold', MoldSchema);
